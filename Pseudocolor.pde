/**
 *    PRODOTTO DA:
 *        Vanzo Luca Samuele,
 *        Simona Di Francesco,
 *        Silvia Cocco.
 *
 * ----------------------------------------------------
 *
 *    ESERCIZIO: Falso Colore, N°2:
 *        Scopo dell'esercizio è di mostrare come 
 *        si può produrre una immagine a colori a 
 *        partire da una immagine in bianco e nero.
 *
 *    Processing 1.5.1
 *    MAC OSX 10.7.X
 */

PImage img;	                      // Immagine
boolean applyFC;                      // VERO | FALSO
int xmin, ymin, xmax, ymax;           // Numerico

/**
 *  FUNZIONE:         SETUP();
 * ----------------------------------------------------
 *  params            NULL
 *  return            VOID
 */
void setup() 
{
  // Carico l'immagine selezionata in IMG.
  img = loadImage(selectInput());
  
  // Imposto la grandezza del frame
  // sulla base delle dimensioni di IMG.
  size(img.width, img.height);  
  
  // Abilito l'Antialiasing (AA).
  smooth();
  
  // Esegui DRAW(); una sola volta
  // e solo in presenza di REDRAW();
  noLoop();
  
  // Disabilito la resa grafica
  // del falso colore in DRAW();
  applyFC = false;
}

/**
 *  FUNZIONE:         DRAW();
 * ----------------------------------------------------
 *  params            NULL
 *  return            VOID
 */
void draw()
{
  // Imposto come sfondo l'immagine caricata.
  background(img);
  
  /**
   *  FalsoColore
   * ----------------------------------------------------
   *  Se applyFC è "vero" allora
   *  creo una porzione di immagine
   *  in pseudocolore e la visualizzo.
   */
  if(applyFC)
  {
    // Controllo coordinate rettangolo.
    EqualizzaCoordinate();
    
    // Salvo l'immagine modificata.
    PImage imageFC = Trasforma(img, xmin, ymin, xmax-xmin, ymax-ymin);
    
    // Disegno la porzione in pseudocolore.
    image(imageFC, xmin, ymin);  
  }
  
  // Disegno il rettangolo di selezione.
  Rettangolo();
}

/**
 *  FUNZIONE:         EQUALIZZACOORDINATE();
 * ----------------------------------------------------
 *  params            NULL
 *  return            VOID
 */
void EqualizzaCoordinate()
{
  /**
   *  A cosa serve?
   * ----------------------------------------------------
   *
   *      +--  YMAX
   *      |      
   *           +-----------------+
   *           |                 |
   *    XMAX   |                 |
   *           |                 |  YMIN
   *           |                 |
   *           +-----------------+
   *                                  |
   *                          XMIN  --+
   *
   * ----------------------------------------------------
   *  Equalizzare è utile quando le 
   *  coordinate finali sono invertite e
   *  quindi possono creare problemi
   *  durante elaborazione del FalsoColore.
   *
   *  Inoltre potrebbe finire oltre i margini 
   *  della figura/frame. Quindi è meglio mettere
   *  dei paletti prima di disegnare ed elaborare.
   */
   
  // Equalizzo l'asse X
  if(xmin > xmax)
  {
    // Scambio i valori.
    xmin = xmin + xmax;        // Y = Y(3) + X(2)
    xmax = xmin - xmax;        // X = Y(5) - X(2)
    xmin = xmin - xmax;        // Y = Y(5) - X(3)
  }                            //     X = 3 e Y = 2
  
  if(xmin < 0) xmin = 0;
  if(xmax > img.width) xmax = img.width - 1;
    
  // Equalizzo l'asse Y
  if(ymin > ymax)
  {
    // Scambio i valori.
    ymin = ymin + ymax;        // Y = Y(3) + X(2)
    ymax = ymin - ymax;        // X = Y(5) - X(2)
    ymin = ymin - ymax;        // Y = Y(5) - X(3)
  }                            //     X = 3 e Y = 2
  
  if(ymin < 0) ymin = 0;
  if(ymax > img.height) ymax = img.height - 1;
}

/**
 *  FUNZIONE:         RETTANGOLO();
 * ----------------------------------------------------
 *  params            NULL
 *  return            VOID
 */
void Rettangolo()
{
  // Bordo rosso.
  stroke(255, 0, 0);
  
  // Nessun riempimento.
  noFill();
  
  // Traccio il rettangolo (x, y, w, h).
  rect(xmin, ymin, xmax-xmin, ymax-ymin);
}

/**
 *  FUNZIONE:         TRASFORMA();
 * ----------------------------------------------------
 *  param1            imageOr:PIMAGE
 *  param2            x:INT
 *  param3            y:INT
 *  param4            larghezza:INT
 *  param5            altezza:INT
 *  return            temp:PIMAGE
 */
PImage Trasforma( PImage imageOr, int x, int y, int larghezza, int altezza )
{
  /**
   *  Creo una nuova immagine vuota su cui lavorare.
   * ----------------------------------------------------
   *  PImage(
   *      x = larghezza,
   *      y = altezza
   *  );
   */
  PImage temp = new PImage(larghezza, altezza);
  
  // Copio la porzione interessata dal rettangolo
  // e carico i pixels per modificarne i componenti.
  temp.copy(imageOr, x, y, larghezza, altezza, 0, 0, larghezza, altezza);
  temp.loadPixels();
  
  /**
   *  Analisi porzione di MATRICE di pixels.
   * ----------------------------------------------------
   *  Struttura immagine visualizzata:
   *  
   *  +----+----+----+----+----+
   *  | 1  | 2  | 3  | 4  | 5  |
   *  +----+----+----+----+----+
   *  | 6  | 7  | 8  | 9  | 10 |
   *  +----+----+----+----+----+
   *  | 11 | 12 | 13 | 14 | 15 |
   *  +----+----+----+----+----+
   *  | 16 | 17 | 18 | 19 | 20 |
   *  +----+----+----+----+----+   Y (Scopo di C1)
   *  | 21 | 22 | 23 | 24 | 25 |
   *  +----+----+----+----+----+
   *                               |
   *        (Scopo di C2) X     -- +
   *
   * ----------------------------------------------------
   *  Struttura immagine in elaborazione:  
   *  
   *  <--- +----+----+----+----+----+----+----+ --->
   *   ... | 10 | 11 | 12 | 13 | 14 | 15 | 16 | ...
   *  <--- +----+----+----+----+----+----+----+ --->
   *
   *  Scopo di C3 è convertire una MATRICE in VETTORE
   *  per rendere i pixels accessibile tramite indice
   *  e non più coordinate come per la matrice.
   */
  for (int y0 = 0; y0 < temp.height; y0++)        // C1
  {
    for (int x0 = 0; x0 < temp.width; x0++)       // C2
    {
      int loc = x0 + (y0*temp.width);             // C3
      
      // Applico lo pseudocolore tramite funzione.
      temp.pixels[loc] = FalsoColore( temp.pixels[loc] );
    }
  }
  
  // Aggiorno i pixels.
  temp.updatePixels();
  
  // Ritorno l'immagine così creata.
  return temp;
}

/**
 *  FUNZIONE:         FALSOCOLORE();
 * ----------------------------------------------------
 *  param1            coloreOr:INT
 *  return            temp:PIMAGE
 */
int FalsoColore( int coloreOr )
{
  // Salvo le singole componenti. 
  float r = red(coloreOr);
  float g = green(coloreOr);
  float b = blue(coloreOr);
  
  // Ottengo il colore in GrayScale
  float gl = (r+g+b)/3;
  
  // Verifico il massimo
  if(gl > 255)
    gl = 255;
  
  // ed il minimo
  if(gl < 0)
    gl = 0;
  
  // Componenti RGB => [ 0 0 GL ]
  if(gl <= 80)
    return color(0, 0, gl*255/80);
  
  // Componenti RGB => [ C GL 255 ]
  else if(gl > 80 && gl < 160)
    return color((gl-80)*255/80, 0, 255);
  
  // Componenti RGB => [ 255 GL 255 ]
  else if(gl >= 160 && gl <= 255)
    return color(255, (gl-160)*255/95, 255);
  
  // Colore di comodo.
  return color(255, 255, 255);
}

/**
 *  FUNZIONE:         MOUSEPRESSED();
 * ----------------------------------------------------
 *  params            NULL
 *  return            VOID
 */
void mousePressed()
{
  // Disabilito la resa grafica
  // del falso colore in DRAW();
  applyFC = false;
  
  // Inizializzo le coordinate del rettangolo.
  xmin = xmax = mouseX;
  ymin = ymax = mouseY;
}

/**
 *  FUNZIONE:         MOUSERELEASED();
 * ----------------------------------------------------
 *  params            NULL
 *  return            VOID
 */
void mouseReleased()
{
  // Salvo la posizione finale del mouse.
  xmax = mouseX;
  ymax = mouseY;
  
  // Abilito la resa grafica
  // del falso colore in DRAW();
  applyFC = true;
  
  // Forzo la funzione DRAW();
  redraw();
}

/**
 *  FUNZIONE:         MOUSEDRAGGED();
 * ----------------------------------------------------
 *  params            NULL
 *  return            VOID
 */
void mouseDragged()
{
  // Salvo man mano per puro
  // fattore estetico durante drag.
  xmax = mouseX;
  ymax = mouseY;
  
  // Forzo la funzione DRAW();
  redraw();
}
